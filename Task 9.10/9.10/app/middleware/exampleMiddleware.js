const urlMiddleware = (request, response, next) => {
    console.log(`Method: ${request.method}  - URL: ${request.url}`);

    next();
}

const timeMiddleware = (request, response, next) => {
    console.log(`Time: ${new Date()}`);

    next();
}
module.exports = { urlMiddleware, timeMiddleware };