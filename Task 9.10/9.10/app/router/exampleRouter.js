const express = require('express');
const { urlMiddleware, timeMiddleware } = require('../middleware/exampleMiddleware');

//Khai báo path
const path = require('path');

// Tạo router
const exampleRouter = express.Router();

// Sử dụng middleware
exampleRouter.use(urlMiddleware, timeMiddleware);



// Sử dụng folder views
exampleRouter.use(express.static(path.join(__dirname + '../../../views')));

//Khai báo API nodeJS
exampleRouter.get('/', (request, response) => {

    response.sendFile(path.join(__dirname + '../../../views/index.html'))
});

module.exports = exampleRouter