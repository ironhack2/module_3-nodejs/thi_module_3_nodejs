//Khai báo thư viện express
const express = require('express');

//  Khai báo router
const exampleRouter = require('./app/router/exampleRouter');


//Khai báo app nodeJS
const app = express();

//Khai báo cổng nodeJS
const port = 8000;

//sử dụng router
app.use('/', exampleRouter);

//Chạy nodeJS
app.listen(port, () => {
    console.log(`App listening on port ${port}`)
});