// IMPORT CÁC CLASS
const User = require('./js/user.js');
const Worker = require('./js/worker.js');
const Student = require('./js/student.js');
const Alumnus = require('./js/alumnus.js');


//KHAI BÁO CỔNG EXPRESS
const express = require('express');


//  KHỞI TẠO APP EXPRESS
const app = express();


// KHAI BÁO CỔNG NODEJS
const port = 8000;


// Khai báo biến User
var user = new User('Hoàng béo', '11/3/1999', 'Việt Trì');
console.log(user.getUser())

// Khai báo biến Worker
var worker = new Worker('Hoàng béo', '11/3/1999', 'Việt Trì', 'Inter', 'IT', '300$');
console.log(worker.getWorker());

// Khai báo biến Student
var student = new Student('Hoàng béo', '11/3/1999', 'Việt Trì', 'Iron Hack', 'J2223', '09123456789');
console.log(student.getStudent());

// Khai báo biến Alumnus
var alumnus = new Alumnus('Hoàng béo', '11/3/1999', 'Việt Trì', 'Iron Hack', 'J2223', '09123456789', 'Lập trình web JavaScript', '113');
console.log(alumnus.getAlumnus());


//  KHAI BÁO API

//GET USER
app.get('/users', (request, response) => {
    response.status(200).json({
        Users: user
    })
});

//GET WORKER
app.get('/workers', (request, response) => {
    response.status(200).json({
        Workers: worker
    })
});

//GET STUDENT
app.get('/students', (request, response) => {
    response.status(200).json({
        Students: student
    })
});

// GET ALUMNUS
app.get('/alumnus', (request, response) => {
    response.status(200).json({
        Alumnus: alumnus
    })
});


//CHẠY APP EXPRESS
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});