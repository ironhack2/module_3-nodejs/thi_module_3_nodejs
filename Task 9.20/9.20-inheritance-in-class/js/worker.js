// Import User
const User = require('./user.js');

//Khai báo class
class Worker extends User {
    constructor(fullName, birthDay, country, job, workplace, salary) {
        super(fullName, birthDay, country);
        this.job = job;
        this.workplace = workplace;
        this.salary = salary;
    }
    getWorker() {
        return {
            "fullName": this.fullName,
            "birthDay": this.birthDay,
            "country": this.country,
            "job": this.job,
            "workplace": this.workplace,
            "salary": this.salary
        }
    }
}

// Export Student
module.exports = Worker;