//  Import Student
const Student = require("./student.js");

//Khai báo class alumnus
class Alumnus extends Student {
    constructor(fullName, birthDay, country, schoolName, className, studentPhone, majors, studentCode) {
        super(fullName, birthDay, country, schoolName, className, studentPhone);
        this.majors = majors;
        this.studentCode = studentCode;
    }
    getAlumnus() {
        return {
            "fullName": this.fullName,
            "birthDay": this.birthDay,
            "country": this.country,
            "schoolName": this.schoolName,
            "className": this.className,
            "studentPhone": this.studentPhone,
            "majors": this.majors,
            "studentCode": this.studentCode
        }
    }
}

//Export Alumnus
module.exports = Alumnus;