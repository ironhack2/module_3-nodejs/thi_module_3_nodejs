// Import User
const User = require('./user.js')

//Khai báo class
class Student extends User {
    constructor(fullName, birthDay, country, schoolName, className, studentPhone) {
        super(fullName, birthDay, country);
        this.schoolName = schoolName;
        this.className = className;
        this.studentPhone = studentPhone;
    }
    getStudent() {
        return {
            "fullName": this.fullName,
            "birthDay": this.birthDay,
            "country": this.country,
            "schoolName": this.schoolName,
            "className": this.className,
            "studentPhone": this.studentPhone
        }
    }
}

// Export Student
module.exports = Student;