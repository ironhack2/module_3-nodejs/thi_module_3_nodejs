class User {
    constructor(fullName, birthDay, country) {
        this.fullName = fullName;
        this.birthDay = birthDay;
        this.country = country;
    }
    getUser() {
        return {
            "fullName": this.fullName,
            "birthDay": this.birthDay,
            "country": this.country
        }
    }
}
module.exports = User;