//  Khai báo class User
class User {
    constructor(Id, Name, Position, Office, Age, StartDate) {
        this.Id = Id;
        this.name = Name;
        this.position = Position;
        this.office = Office;
        this.age = Age;
        this.startDate = StartDate;
    }
    getUser() {
        return {
            "ID": this.Id,
            "name": this.name,
            "position": this.position,
            "office": this.office,
            "age": this.age,
            "startDate": this.startDate
        }
    }
    getAge() {
        return this.age
    }
    getId() {
        return this.Id
    }
}

var user_1 = new User(1, "Airi Satou", "Accountant", "Tokyo", "33", "2008/11/28");
var user_2 = new User(2, "Angelica Ramos", "Chief Executive Officer (CEO)", "London", "47", "2009/10/09");
var user_3 = new User(3, "Ashton Cox", "Junior Technical Author", "San Francisco", "66", "2009/01/12");
var user_4 = new User(4, "Bradley Greer", "Software Engineer", "London", "41", "2012/10/13");
var user_5 = new User(5, "Brenden Wagner", "Software Engineer", "San Francisco", "28", "2011/06/07");
var user_6 = new User(6, "Brielle Williamson", "AIntegration Specialist", "New York", "61", "2012/12/02");
var user_7 = new User(7, "Bruno Nash", "Software Engineer", "London", "38", "2011/05/03");
var user_8 = new User(8, "Caesar Vance", "Pre-Sales Support", "New York", "21", "2011/12/12");
var user_9 = new User(9, "Cara Stevens", "Sales Assistant", "New York", "46", "2011/12/06");
var user_10 = new User(10, "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", "22", "2012/03/29");

var allUser = [user_1, user_2, user_3, user_4, user_5, user_6, user_7, user_8, user_9, user_10];

// Export User
module.exports = { allUser };