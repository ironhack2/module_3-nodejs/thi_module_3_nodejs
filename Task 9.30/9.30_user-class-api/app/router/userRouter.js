//KHAI BÁO THƯ VIỆN EXPRESS
const express = require('express');
const { userMiddleware } = require('../middleware/userMiddleware');
const { allUser } = require('../../../9.30_user-class-api/data');


//TẠO ROUTER
const userRouter = express.Router();

//  SỬ DỤNG MIDDLEWARE
userRouter.use(userMiddleware);



// GET ALL USER
userRouter.get('/users', (request, response) => {
    response.status(200).json({
        Users: allUser
    })
})

// GET USER BY AGE
userRouter.get('/users/search', (request, response) => {
    let queryRequest = request.query;

    if (queryRequest.age == "") {
        return response.status(200).json({
            Users: allUser
        })
    }

    if (queryRequest.age != "") {
        let resultUserByAge = [];

        let filterUser = allUser.filter(paramAge => paramAge.getAge() > queryRequest.age)

        resultUserByAge.push(filterUser);
        response.status(200).json({
            Users: resultUserByAge
        })
    }
})


// GET USER BY ID
userRouter.get('/users/:userId', (request, response) => {
    let userId = request.params.userId;

    if (isNaN(userId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            Message: "Không có ID này!!!"
        })
    } else {
        allUser.filter(paramID => {
            if (userId == paramID.getId()) {
                response.status(200).json({
                    Users: paramID.getUser()
                })
            }
        })
    }

})


//CREATE A USER
userRouter.post('/users', (request, response) => {
    let body = request.body;

    console.log("Create a user");
    console.log(body);
    response.json({
        ...body
    })
});


//UPDATE A USER
userRouter.put('/users/:userId', (request, response) => {
    let id = request.params.userId;
    let body = request.body;

    console.log("Update a user");
    console.log({ id, ...body });
    response.json({
        message: { id, ...body }
    })
});


//DELETE A USER
userRouter.delete('/users/:userId', (request, response) => {
    let id = request.params.userId;

    console.log(`delete a user ${id}`);
    response.json({
        message: `delete a user ${id}`
    })
});


//    EXPORT USER ROUTER
module.exports = { userRouter };